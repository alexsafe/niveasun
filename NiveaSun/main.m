//
//  main.m
//  NiveaSun
//
//  Created by Bogdan-Nicolae Oprea on 5/24/14.
//  Copyright (c) 2014 Nivea. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

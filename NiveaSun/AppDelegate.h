//
//  AppDelegate.h
//  NiveaSun
//
//  Created by Bogdan-Nicolae Oprea on 5/24/14.
//  Copyright (c) 2014 Nivea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
